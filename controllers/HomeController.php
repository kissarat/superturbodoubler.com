<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;


use app\behaviors\Access;
use app\modules\invoice\models\Invoice;
use app\models\User;
use app\helpers\SQL;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class HomeController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'manager' => ['settings'],
            ]
        ];
    }

    public function actionIndex() {
        return $this->render('index', [
            'statistics' => $this->renderPartial('statistics', ['statistics' => static::statistics()])
        ]);
    }

    public function actionSettings() {
        $settings = SQL::queryArray('SELECT * FROM var ORDER BY name');
        if (!empty($_POST)) {
            foreach($_POST as $key => $value) {
                if ('_' == $key[0]) {
                    continue;
                }
                $params = [
                    ':name' => $key,
                    ':value' => empty($value) ? null : $value
                ];

                if (!isset($settings[$key])) {
                    SQL::query('INSERT INTO var VALUES (:name, :value)', $params);
                }
                elseif ($value != $settings[$key]) {
                    SQL::query('UPDATE var SET value = :value WHERE name = :name', $params);
                }

                $settings[$key] = $value;
            }
        }
        return $this->render('settings', [
            'settings' => $settings
        ]);
    }

    public function actionError() {
        $exception = Yii::$app->getErrorHandler()->exception;
        $message = $exception->getMessage();
        if ($exception instanceof ForbiddenHttpException) {
            $message = $message ? Yii::t('app', $message) : Yii::t('app', 'Forbidden');
        }
        if ($message) {
            Yii::$app->session->setFlash('error', $message);
        }
        return $this->render('error', [
            'exception' => $exception
        ]);
    }

    public static function statistics() {
        $started = strtotime(SQL::queryCell('SELECT "time" FROM "journal" WHERE id = 1'));
        $invested = (int) SQL::queryCell('SELECT count(*) FROM "node"');
        return [
            'Started' => date('d-m-Y', $started),
            'Running days' => floor((time() - $started)/(3600 * 24)),
            'Users' => User::find()->count(),
            'Total deposited' => Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount > 0')->sum('amount'),
            'Total withdraw' => - Invoice::find()->where(['or',
                ['status' => 'success'],
                ['status' => 'delete']
            ])->andWhere('amount < 0')->sum('amount'),
            'Number of investments' => $invested
        ];
    }
}
