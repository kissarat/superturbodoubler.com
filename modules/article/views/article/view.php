<?php
/**
 * @link http://zenothing.com/
 */

use app\widgets\Ext;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\article\models\Article */

if (!isset($title)) {
    $title = null;
}

if (null === $title) {
    $title = $model->title;
}

if (false !== $title) {
    $this->title = $title;
    $this->registerMetaTag([
        'name' => 'description',
        'content' => substr(strip_tags($model->content), 0, 150) . '...'
    ]);
}

/*
if (!$model->name) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $title;
}
*/
?>
<article class="article-view middle" itemscope itemtype="//schema.org/Article">
    <?php
    if (false !== $title) {
        echo Ext::stamp();
        echo Html::tag('h1', Html::encode($this->title), ['itemprop' => 'name']);
    }
    ?>
    <p class="form-group">
        <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()): ?>
            <?= Html::a(Yii::t('app', 'Update'), ['/article/article/update', 'id' => $model->id],
                ['class' => 'btn btn-primary']); ?>

            <?php
            if (!$model->name) {
                echo Html::a(Yii::t('app', 'Delete'), ['/article/article/delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]);
            }
            ?>
        <?php endif ?>
    </p>

    <div itemprop="articleBody">
        <?= $model->content ?>
    </div>

</article>
