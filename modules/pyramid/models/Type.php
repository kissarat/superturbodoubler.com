<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\pyramid\models;

use Yii;
use yii\base\Model;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 *
 * @property integer $id
 * @property string $name
 * @property number $stake
 * @property number $income
 * @property number $profit
 * @property number $bonus
 * @property integer $degree
 */
class Type extends Model
{
    private static $_all;
    public $id;
    public $name;
    public $stake;
    public $income;
    public $profit;
    public $bonus;
    public $degree;
    public $visible;
    public $reinvest;
    public $next_id;

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'stake' => Yii::t('app', 'Stake'),
            'income' => Yii::t('app', 'Income'),
            'reinvest' => Yii::t('app', 'Reinvest'),
            'next_id' => Yii::t('app', 'Next'),
            'bonus' => Yii::t('app', 'Bonus'),
        ];
    }

    /**
     * @param $id
     * @return Type|null
     */
    public static function get($id) {
        $types = Type::all();
        return isset($types[$id]) ? $types[$id] : null;
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * @return array
     */
    public static function getItems() {
        $items = [];
        foreach(static::all() as $type) {
            if ($type->visible) {
                $items[$type->id] = Yii::t('app', $type->name);
            }
        }
        return $items;
    }

    /**
     * @return integer
     */
    public function getProfit() {
        return $this->income ? $this->stake * 2 - $this->income : 0;
    }

    public function getReinvest() {
        return $this->stake;
    }

    public static function create(array $type) {
        if (empty($type['degree'])) {
            $type['degree'] = 5;
        }
        if (empty($type['visible'])) {
            $type['visible'] = true;
        }
        if (empty($type['bonus'])) {
            $type['bonus'] = 5;
        }
        return static::$_all[$type['id']] = new Type($type);
    }

    /**
     * @return Type[]
     */
    public static function all() {
        if (!static::$_all) {
            Type::create([
                'id' => 1,
                'name' => 'Super',
                'stake' => 25,
                'income' => 25,
                'next_id' => 2,
                'reinvest' => null
            ]);

            Type::create([
                'id' => 2,
                'name' => 'Turbo',
                'stake' => 100,
                'income' => 100,
                'next_id' => 3,
                'reinvest' => 2
            ]);

            Type::create([
                'id' => 3,
                'name' => 'Super-VIP',
                'stake' => 250,
                'income' => 400,
                'next_id' => 4,
                'reinvest' => 3
            ]);

            Type::create([
                'id' => 4,
                'name' => 'Doubler',
                'stake' => 500,
                'income' => 900,
                'next_id' => 5,
                'reinvest' => 4
            ]);

            Type::create([
                'id' => 5,
                'name' => 'Turbo-VIP',
                'stake' => 1000,
                'income' => 4800,
                'next_id' => null,
                'reinvest' => null
            ]);
        }
        return static::$_all;
    }
}
