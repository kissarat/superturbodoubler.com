<?php
/**
 * @link http://zenothing.com/
 * @var yii\web\View $this
 * @var string $statistics
 * @var string $marketing
 */

use app\modules\article\models\Article;
use app\modules\feedback\controllers\FeedbackController;
use app\modules\pyramid\models\Type;
use app\widgets\Ext;
use yii\helpers\Html;
use yii\web\JqueryAsset;

$this->title = Yii::$app->name;

$benefits = [
    'Доступный вход',
    'Никаких приглашений',
    'Моментальные выплаты'
];

$this->registerMetaTag([
    'name' => 'description',
    'content' => implode(' ⬤ ', $benefits)
]);

/**
 * @param string $name
 * @param boolean|string $title
 * @return string
 */
function article($name, $title = null) {
    $model = Article::findOne(['name' => $name]);
    $content = Yii::$app->view->renderFile('@app/modules/article/views/article/view.php', [
        'model' => $model,
        'title' => $title
    ]);

    return null === $title ? $content : [
        'model' => $model,
        'content' => $content
    ];
}

$steps_title = [];
$steps_content = [];
foreach(['step1', 'step2', 'step3'] as $step) {
    $article = article($step, false);
    $steps_title[] = Html::tag('th', $article['model']->title);
    $steps_content[] = Html::tag('td', $article['content']);
}

?>
<div class="home-index">
    <?= Ext::stamp() ?>

    <div class="index-row">
        <div class="border">


            <div class="quo">
                <h1>Преимущества</h1>
                <table>
                    <tr>
                        <td><div id="one">ОБЩАЯ ОЧЕРЕДЬ!
                                КАЖДЫЙ ТИП СТОЛА ЯВЛЯЕТСЯ ОБЩЕЙ ОЧЕРЕДЬЮ...
                                РАБОТАЕТ ПРИНЦИП "ВСЕ-ПОМОГАЮТ-ВСЕМ!" </div></td><td><div id="two">РЕИНВЕСТ В КАЖДОМ СТОЛЕ!

                                ПОСЛЕ ВЫХОДА ИЗ СТОЛА, ВЫ АВТОМАТИЧЕСКИ ВОЗВРАЩАЕТЕСЬ
                                ВНИЗ ОЧЕРЕДИ В ЭТОТ ЖЕ СТОЛ!
                                ТЕМ САМЫМ ВЫ ВО ВСЕХ СТОЛАХ БУДЕТЕ ИМЕТЬ ДОПОЛНИТЕЛЬНОЕ МЕСТО,
                                ЧТО ДАЕТ ВАМ ВОЗМОЖНОСТЬ ЗАРАБАТЫВАТЬ ПОСТОЯННО!</div></td><td><div id="three">ПАССИВНЫЙ ДОХОД!
                                ДАЖЕ ЕСЛИ ВЫ НИКОГО НЕ ПРИГЛАСИЛИ,
                                СТОЛЫ БУДУТ ЗАКРЫВАТЬСЯ БЛАГОДАРЯ ОБЩЕЙ ОЧЕРЕДИ!</div></td>
                    </tr>
                    <tr>
                        <td><div id="four">АКТИВНЫЙ ДОХОД!
                                ПРЕДУСМОТРЕНА РЕФЕРАЛЬНАЯ СИСТЕМА-5% ОТ СУММЫ ВХОДА ВАШЕГО ПРИГЛАШЕННОГО,
                                НА ЛЮБОЙ ИЗ СТОЛОВ!</div></td><td><div id="five">ВАША СТАБИЛЬНОСТЬ!
                                ПОЧУСТВУЙТЕ ПО-НАСТОЯЩЕМУ СТАБИЛЬНЫЙ ДОХОД,
                                ОТ ПОСТОЯННОГО ВЫХОДА И РЕИНВЕСТА ВО ВСЕХ ПРОГРАММАХ!</div></td><td><div id="six"><b>ОСТАВАЙТЕСЬ С НАМИ!<br>ВМЕСТЕ МЫ СИЛА!</b></div></td>
                    </tr>
                </table>
            </div>


        </div>
        <div class="stat">
            <h1>Статистика</h1>
            <?= $statistics ?>
        </div>
        <div class="plans">
            <div>
                <?= article('marketing') ?>
            </div>
            <?php if (Yii::$app->user->isGuest): ?>
            <div class="more button"><?= Html::a(Yii::t('app', 'Signup'), ['user/signup'], ['class' => 'signup']) ?></div>
        </div>
        <?php endif; ?>
        <div></div>
    </div>

</div>
