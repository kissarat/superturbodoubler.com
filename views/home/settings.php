<?php
/**
 * @link http://zenothing.com/
 * @var string[] $settings
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

echo Html::beginTag('div');
$form = ActiveForm::begin();
foreach($settings as $key => $value) {
    if ('_' != $key[0]) {
        echo Html::tag('div',
            Html::label(Yii::t('app', $key)) .
            Html::textInput($key, $value, ['class' => 'form-control']),
            ['class' => 'form-group']);
    }
}

echo Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']);
ActiveForm::end();
echo Html::endTag('div');

