<?php
/**
 * @link http://zenothing.com/
 */

use app\helpers\SQL;
use app\widgets\Alert;
use app\helpers\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $settings string[] */
$settings = SQL::queryArray('SELECT * FROM var ORDER BY name');
$social_url = 'http://' . $_SERVER['HTTP_HOST'];

function addtomany($network) {
    return "http://www.addtoany.com/add_to/$network?" . http_build_query([
        'linkname' => Yii::$app->name,
        'linkurl' => 'http://' . $_SERVER['HTTP_HOST']
    ]);
}

AppAsset::register($this);
$login = Yii::$app->user->isGuest ? '' : 'login';
$manager = !Yii::$app->user->isGuest && Yii::$app->user->identity->isManager();

$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="image_src" href="/images/logo.png" />
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body class="<?= Yii::$app->controller->id ?> <?= Yii::$app->controller->action->id ?>">
<?php
$this->beginBody();
?>
<header>
    <?php if ('home' == Yii::$app->controller->id && 'index' == Yii::$app->controller->action->id): ?>
        <div class="headcont">
            <div class="hd">
                <span>Super</span> <span><b>Turbo</b> </span> <span>Doubler</span>
            </div>
            <?php if (Yii::$app->user->isGuest): ?>
                <div class="login button">
                    <?= Html::a(Yii::t('app', 'Login'), ['user/login'], ['class' => 'signup']) ?>
                </div>

                <div class="login button">
                    <?= Html::a(Yii::t('app', 'Signup'), ['user/signup'], ['class' => 'signup']) ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</header>
<?php

NavBar::begin();

$items = [
    ['label' => Yii::t('app', ''), 'options' => ['class'=>'logo']],
    ['label' => Yii::t('app', 'Home'), 'url' => ['/home/index'], 'options' => ['class' => 'hideable']],
    ['label' => Yii::t('app', 'Feedback'), 'url' => ['/feedback/feedback/' . ($manager ? 'index' : 'create')]],
    ['label' => Yii::t('app', 'FAQ'), 'url' => ['/faq/faq/index']],
    ['label' => Yii::t('app', 'News'), 'url' => ['/article/article/index']]
];

if (Yii::$app->user->getIsGuest()) {
    $items[] = ['label' => Yii::t('app', 'Signup'), 'url' => ['/user/signup']];
    $items[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/user/login']];
}
else {
    if ($manager) {
        $items[] = ['label' => Yii::t('app', 'Journal') , 'url' =>['/journal/index']];
        $items[] = ['label' => Yii::t('app', 'Investments'), 'url' => ['/pyramid/node/index']];
        $items[] = ['label' => Yii::t('app', 'Marketing'), 'url' => ['/pyramid/type/index']];
        $items[] = ['label' => Yii::t('app', 'Translation') , 'url' => ['/lang/lang/index']];
    }
    $items[] = ['label' => Yii::t('app', 'Income'), 'url' => ['/pyramid/income/index']];
    $items[] = ['label' => Yii::t('app', 'Payments') , 'url' =>['/invoice/invoice/index']];
    $items[] = ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/view']];
    $items[] = ['label' => Yii::t('app', 'Logout'), 'url' => ['/user/logout']];
}

if (Yii::$app->user->getIsGuest() || !Yii::$app->user->identity->isManager()) {
    $items[] = 'ru' == Yii::$app->language
        ? ['label' => 'EN', 'url' => ['/lang/lang/choice', 'code' => 'en'], 'options' => ['title' => 'English']]
        : ['label' => 'RU', 'url' => ['/lang/lang/choice', 'code' => 'ru'], 'options' => ['title' => 'Русский']];
}

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => $items,
]);
NavBar::end();
?>


<div class="wrap <?= $login ?>">
    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => false,
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>

        <?= $content ?>
    </div>
</div>

<div id="linux">
    <div>
        <img src="/images/linux.png" />
    </div>
    <?= Html::tag('div', Yii::t('app', 'Welcome, Linux user. We are glad you use open source software!'), [
        'class' => 'welcome'
    ]) ?>
    <div class="glyphicon glyphicon-remove"></div>
</div>

<footer class="footer">
    <?php
    $contacts = [
        Html::a(Html::img('/images/skype.png', ['title' => Yii::t('app', 'Skype call')]), 'skype:' . $settings['skype']),
        Html::a($settings['email'], 'email:' . $settings['email']),
    ];

    if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->isManager()) {
        $contacts[] = Html::a(Yii::t('app', 'Update'), ['/home/settings']);
    }

    echo Html::tag('div', implode("\n", $contacts), ['class' => 'btn btn-primary']);
    ?>

    <div class="signature">
        <p><img src="images/logo.png" class="flogo"></p>
        <span>Super</span><span>Turbo</span><span>Doubler</span>
    </div>

    <?= Html::tag('div', implode("\n", [
        Html::a(Html::img('/images/vkontakte.png', ['alt' => 'Вконтакте']),
            "https://vk.com/share.php?" . http_build_query(['url' => $social_url])),
        Html::a(Html::img('/images/facebook.png', ['alt' => 'Facebook']),
            'https://www.facebook.com/sharer.php?' . http_build_query([
                's' => 100,
                'p[url]' => $social_url
            ])),
        Html::a(Html::img('/images/odnoklassniki.png', ['alt' => 'Одноклассники']),
            addtomany('odnoklassniki')),
        Html::a(Html::img('/images/twitter.png', ['alt' => 'Twitter']),
            'https://twitter.com/intent/tweet?' . http_build_query(['url' => $social_url])),
        ''
    ])); ?>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
