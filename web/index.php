<?php
/**
 * @link http://zenothing.com/
 */
define('CONFIG', __DIR__ . '/../config');

require CONFIG . '/boot.php';
require CONFIG . '/web.php';

(new yii\web\Application($config))->run();
